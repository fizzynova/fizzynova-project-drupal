<?php
global $base_root;
$tDir = $base_root . '/' . drupal_get_path('theme', '{project-system-name}');

?>
<?php
if ($messages) {
?>
<div class="message-container">
    <?php print($messages); ?>
</div>
<?php
}
?>
<div class="row">
    <div class="col-sm-12">
        <?php print render($page['content']); ?>
    </div>
</div>
